import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

// AIzaSyBGXcAz5NCiKHSMHQ3E4Dt9ugK0xkqbxiY
// client id - 202816744941-23t5fg469o2l2r8ko5egigvj14lo57mq.apps.googleusercontent.com
// client scret- BBK0xO2MW8Rqn4zT3Qhj-G0b