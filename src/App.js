import React from 'react';
import MyApp from './components/MyApp';
import StatusSign from './components/StatusSign';
import Main from './components/Main';

function App() {
  return (
    <div className="App">
    <h1>hi</h1>
     <MyApp/>
     <StatusSign/>
     <Main/>
    </div>
  );
}

export default App;
