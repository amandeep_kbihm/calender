import React, { Component } from 'react'
import Calendar from 'react_google_calendar'

const calendar_configuration = {
    api_key: 'AIzaSyBGXcAz5NCiKHSMHQ3E4Dt9ugK0xkqbxiY',
    calendars: [
      {
        name: 'demo', // whatever you want to name it
        url: '5q81nn5buesf517ck82hb68cqo@group.calendar.google.com' // your calendar URL
      }
    ],
    dailyRecurrence: 700,
    weeklyRecurrence: 500,
    monthlyRecurrence: 20
}

export default class MyApp extends Component {
    constructor(props) {
      super(props)
        this.state = {
          events: []
        }
    }

    render = () =>
      <div>
        <Calendar
          events={this.state.events}
          config={calendar_configuration} />
      </div>
}